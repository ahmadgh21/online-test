#!/usr/bin/env bash
function TrapError {
	local parent_lineno="$1"
  	local message="$2"
  	local code="${3:-1}"
  	if [[ -n "$message" ]] ; then
    	echo "Error on or near line ${parent_lineno}: ${message}; exiting with status ${code}"
 	else
    	echo "Error on or near line ${parent_lineno}; exiting with status ${code}"
  	fi
  	exit "${code}"
}

function TrapExit {
	rv=$?
	exit $rv
}

trap 'TrapError $LINENO' ERR
trap TrapExit EXIT

cluster_name="$KUBERNETES_CLUSTER_NAME"
cluster_zone="$KUBERNETES_CLUSTER_ZONE"
cluster_project_id="$KUBERNETES_PROJECT_ID"

if [ -z "$cluster_name" ]
then
    echo "cluster name is not set"
    exit 1
fi

if [ -z "$cluster_zone" ]
then
    echo "cluster zone is not set"
    exit 1
fi

if [ -z "$cluster_project_id" ]
then
    echo "cluster project id is not set"
    exit 1
fi

gcloud container clusters get-credentials $cluster_name --zone $cluster_zone --project $cluster_project_id
kubectl delete -k ./