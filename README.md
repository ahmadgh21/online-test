## Prerequisite

1. Download gcloud cli at url below:
    `https://cloud.google.com/sdk/docs/#install_the_latest_cloud_tools_version_cloudsdk_current_version`

2. Extract the downloaded file and navigate to extracted folder and run `./google-cloud-sdk/install.sh`

3. run gcloud init to initialize SDK `./google-cloud-sdk/bin/gcloud init`

4. Install kubectl `gcloud components install kubectl`

5. run `gcloud container clusters get-credentials <your_cluster> --zone <your_zone> --project <your_project_id>` to get credentials for kubectl. Make sure Google Kubernetes Engine is activated for your google account.

## Deployment

run `KUBERNETES_CLUSTER_NAME=<your_cluster_name> KUBERNETES_CLUSTER_ZONE=<your_cluster_zone> KUBERNETES_PROJECT_ID=<your_cluster_project_id> ./spin-up.sh`

## Cleaning Up

run `KUBERNETES_CLUSTER_NAME=<your_cluster_name> KUBERNETES_CLUSTER_ZONE=<your_cluster_zone> KUBERNETES_PROJECT_ID=<your_cluster_project_id> ./clean-up.sh`





